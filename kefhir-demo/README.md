# Kefhir example project

### Create database if needed:
```
docker run -d --restart=unless-stopped --name kefhir-postgres -e POSTGRES_PASSWORD=postgres -p 5151:5432 postgres:14

docker exec -i kefhir-postgres psql -U postgres <<-EOSQL
CREATE ROLE kefhir_admin LOGIN PASSWORD 'test' NOSUPERUSER INHERIT NOCREATEDB CREATEROLE NOREPLICATION;
CREATE ROLE kefhir_app   LOGIN PASSWORD 'test' NOSUPERUSER INHERIT NOCREATEDB CREATEROLE NOREPLICATION;
CREATE DATABASE kefhirdb WITH OWNER = kefhir_admin ENCODING = 'UTF8' TABLESPACE = pg_default CONNECTION LIMIT = -1;
grant temp on database kefhirdb to kefhir_app;
grant connect on database kefhirdb to kefhir_app;
EOSQL
```

### Run application:
```
./gradlew run
```
or
```
docker run -d --name=kefhir
  -eDB_URL='jdbc:postgresql://172.17.0.1:5151/kefhirdb'
  -p 8181:8181 docker.kodality.com/kefhir-demo
```
Env variables:
```
DB_URL #database jdbc url (jdbc:postgresql://localhost:5151/kefhirdb)
DB_POOL_SIZE
DB_APP_USER #database app user, for data read and insert
DB_APP_PASSWORD
DB_ADMIN_USER #database admin user, for altering database (migration, new resource definitions, new indexes)
DB_ADMIN_PASSWORD
DEFS_URL #link to definitions zip.
```
