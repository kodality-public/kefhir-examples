package com.kodality.kefhir;

import io.micronaut.runtime.Micronaut;

public class KefhirExampleApplication {

  public static void main(String[] args) {
    Micronaut.run(KefhirExampleApplication.class);
  }
}
