package com.kodality.kefhir;

import com.kodality.kefhir.core.api.conformance.HapiValidationSupportProvider;
import io.micronaut.context.annotation.Bean;
import io.micronaut.context.annotation.Factory;
import org.hl7.fhir.common.hapi.validation.support.RemoteTerminologyServiceValidationSupport;

@Factory
public class ValidationSupportFactory {
//  @Bean
  public HapiValidationSupportProvider getRemoteTerminologyServiceValidationSupport() {
    return context -> new RemoteTerminologyServiceValidationSupport(context, "https://terminology.kodality.dev/api/fhir");
  }
}
