# Kefhir experiments project:
total 3 separate databases: store, search, patient

### Create database if needed:
```
docker rm -vf kefhir-store-postgres kefhir-search-postgres

docker run -d -e TZ=Europe/Tallinn --restart=unless-stopped --name kefhir-store-postgres -p 5190:5432 docker.kodality.com/postgres-docker:14
docker exec -e "DB_NAME=kefhirdb" -e "USER_PREFIX=kefhir" kefhir-store-postgres /opt/scripts/createdb.sh

docker run -d -e TZ=Europe/Tallinn --restart=unless-stopped --name kefhir-search-postgres -p 5191:5432 docker.kodality.com/postgres-docker:14
docker exec -e "DB_NAME=kefhirdb" -e "USER_PREFIX=kefhir" kefhir-search-postgres /opt/scripts/createdb.sh

docker run -d -e TZ=Europe/Tallinn --restart=unless-stopped --name kefhir-patient-postgres -p 5192:5432 docker.kodality.com/postgres-docker:14
docker exec -e "DB_NAME=kefhirdb" -e "USER_PREFIX=kefhir" kefhir-patient-postgres /opt/scripts/createdb.sh
```

### Run application:
```
./gradlew run
#if needed, run:
https://kexus.kodality.com/repository/store-public/kefhir/download-fhir-definitions.sh
```
