package com.kodality.kefhir.medication;

import com.kodality.kefhir.rest.BaseFhirResourceServer;
import com.kodality.kefhir.rest.model.KefhirRequest;
import com.kodality.kefhir.rest.model.KefhirResponse;
import jakarta.inject.Singleton;
import org.hl7.fhir.r5.model.CodeableConcept;
import org.hl7.fhir.r5.model.Coding;
import org.hl7.fhir.r5.model.Medication;
import org.hl7.fhir.r5.model.ResourceType;

@Singleton
public class MedicationController extends BaseFhirResourceServer {

  @Override
  public String getTargetType() {
    return ResourceType.Medication.name();
  }

  @Override
  public KefhirResponse read(KefhirRequest req) {
    Medication med = new Medication();
    med.setCode(new CodeableConcept().addCoding(new Coding().setCode("dummy")));
    med.setId(req.getPath());
    return new KefhirResponse(200, med);
  }

  @Override
  public KefhirResponse searchCompartment(KefhirRequest req) {
    return null;
  }
}
