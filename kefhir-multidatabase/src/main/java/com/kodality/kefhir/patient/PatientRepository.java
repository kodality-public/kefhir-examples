package com.kodality.kefhir.patient;

import com.kodality.kefhir.store.repository.ResourceRepository;
import jakarta.inject.Named;
import jakarta.inject.Singleton;
import org.springframework.jdbc.core.JdbcTemplate;

@Singleton
public class PatientRepository extends ResourceRepository {

  public PatientRepository(@Named("patientStoreAppJdbcTemplate") JdbcTemplate jdbcTemplate) {
    super(jdbcTemplate);
  }
}
