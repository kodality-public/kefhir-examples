package com.kodality.kefhir.patient;

import com.kodality.kefhir.store.PgResourceStorage;
import jakarta.inject.Singleton;
import org.hl7.fhir.r4.model.ResourceType;

@Singleton
public class PatientStorage extends PgResourceStorage {
  public PatientStorage(PatientRepository patientRepository) {
    super(patientRepository);
  }

  @Override
  public String getResourceType() {
    return ResourceType.Patient.name();
  }
}
