/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kodality.kefhir.patient;

import jakarta.inject.Named;
import jakarta.inject.Singleton;
import org.springframework.jdbc.core.JdbcTemplate;

@Singleton
public class ResourceFunctionsRepository {
  private final JdbcTemplate adminJdbcTemplate;

  public ResourceFunctionsRepository(@Named("storeAdminJdbcTemplate") JdbcTemplate adminJdbcTemplate) {
    this.adminJdbcTemplate = adminJdbcTemplate;
  }

  public void defineResource(String type) {
    adminJdbcTemplate.queryForObject("select store.define_resource(?)", String.class, type);
  }
}
