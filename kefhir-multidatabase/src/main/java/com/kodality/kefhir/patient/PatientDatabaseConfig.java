package com.kodality.kefhir.patient;

import com.kodality.kefhir.PgTransactionManager;
import io.micronaut.context.annotation.Bean;
import io.micronaut.context.annotation.Factory;
import jakarta.inject.Named;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;

@Factory
public class PatientDatabaseConfig {

  @Bean
  @Named("patientStoreAppJdbcTemplate")
  public JdbcTemplate storeAppJdbcTemplate(@Named("patient-store-app") DataSource ds) {
    return new JdbcTemplate(ds);
  }

  @Bean
  public PgTransactionManager transactionManager(@Named("patient-store-app") DataSource ds) {
    return new PgTransactionManager(ds);
  }

  @Bean
  @Named("patientStoreAdminJdbcTemplate")
  public JdbcTemplate storeAdminJdbcTemplate(@Named("patient-store-admin") DataSource ds) {
    return new JdbcTemplate(ds);
  }

}
