package com.kodality.kefhir;

import io.micronaut.runtime.Micronaut;

public class KefhirExperimentsApplication {

  public static void main(String[] args) {
    Micronaut.run(KefhirExperimentsApplication.class);
  }
}
