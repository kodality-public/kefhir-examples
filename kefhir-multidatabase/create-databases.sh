docker rm -vf kefhir-store-postgres kefhir-search-postgres kefhir-patient-postgres

docker run -d -e TZ=Europe/Tallinn --restart=unless-stopped --name kefhir-store-postgres -p 5190:5432 docker.kodality.com/postgres-docker:14
docker run -d -e TZ=Europe/Tallinn --restart=unless-stopped --name kefhir-search-postgres -p 5191:5432 docker.kodality.com/postgres-docker:14
docker run -d -e TZ=Europe/Tallinn --restart=unless-stopped --name kefhir-patient-postgres -p 5192:5432 docker.kodality.com/postgres-docker:14

sleep 3

docker exec -e "DB_NAME=kefhirdb" -e "USER_PREFIX=kefhir" kefhir-store-postgres /opt/scripts/createdb.sh
docker exec -e "DB_NAME=kefhirdb" -e "USER_PREFIX=kefhir" kefhir-search-postgres /opt/scripts/createdb.sh
docker exec -e "DB_NAME=kefhirdb" -e "USER_PREFIX=kefhir" kefhir-patient-postgres /opt/scripts/createdb.sh

